<?php

use Illuminate\Database\Seeder;

class LeaderboardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            "Emma",
            "Noah",
            "James",
            "William",
            "Olivia",
        ];

        foreach($users as $user) {
            DB::table('players')->insert([
                'name' => $user,
                'score' => 0,
            ]);
        }
    }
}
