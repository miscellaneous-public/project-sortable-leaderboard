<p align="center">
	<img src="https://i.imgur.com/Mfy1UaM.png" width="350">
</p>

<p align="center">
	<a href="https://gitlab.com/miscellaneous-public/project-sortable-leaderboard/-/commits/master"><img alt="pipeline status" src="https://gitlab.com/miscellaneous-public/project-sortable-leaderboard/badges/master/pipeline.svg" /></a>
</p>

# Sortable Leaderboard

The purpose of this project is to demonstrate my knowledge of frontend and backend while demonstrating other fancy-schmancy abilities. :)

Instructions for installing this app can be found below.

## Acceptance Criteria

- You have 5 users showing in a leaderboard
- All users start with 0 points
- As you click +/-, the leaderboard updates and users are re-ordered based on score 
- Names sorted alphabetically if they are tied 

## Technologies

- Laravel
- VueJS
- Jest

## What this assignment demonstrates

- Experience with frameworks
  - Laravel
  - Vue
- Experience with BE and FE tooling
  - Composer
  - NPM (or Yarn)
  - Webpack
  - SASS
- Experience with unit testing
- Experience with CI pipelines
  - Linting
  - Security audits on vendor packages
- That I like to have a bit of fun 😊

## Installation

You will need to first make sure to set up an up-to-date `Laravel Homestead` - [link](https://laravel.com/docs/7.x/homestead) (you could probably use `Laravel Valet` or even run it natively using `php artisan serve`, but I haven't tested this)

Once you have mapped the application to your vagrant box as per standard Homestead setup, run:
- `composer install` (installs PHP vendor dependencies)
  
Then, you will need to adjust the `.env` file to map the location of the `db.sqlite` file.

![.env](https://i.imgur.com/9y8USbZ.png)

After this, you should only need to run the following commands:
  - `php artisan migrate` (migrates the database)
  - `php artisan db:seed` (seeds the database with players)
  - `npm run dev` (builds the front-end app)

After this, you should, like with any Homestead app, be able to visit the application in your browser using the host you specified in Homestead followed by `/leaderboard`.

In my case, it is: `http://canada-drives-interview-homework.local/leaderboard`.

## Extra features

### Unit testing

This application sorts the leaderboard dynamically according to the rules given. Using Jest, I have a unit test for this (`resources/js/tests/index.spec.js`).

To execute this test, run `npm run test`.

![Unit test](https://i.imgur.com/L7Dazxn.png)

### Security auditing for vendor packages

We live in a world where we utilize a lot of open source code. It's a good idea to stay on top of things from a security perspective.

The CI pipeline audits the vendor packages for known issues for both PHP and JS packages.

Example:

![npm audit](https://i.imgur.com/ynqn4QC.png)

(The above got fixed in this project, FYI :p)

### CI Pipeline

This project utilizes `GitLab`'s CI pipeline.

Have a look at `.gitlab-ci.yml`.

What it effectively does is, upon me pushing to any branch in GitLab, it spins up a series of docker containers in sequence to:
  - build the application
  - lint code
  - check vendor packages for security vulnerabilities
  - run my unit tests

![CI Pipeline](https://i.imgur.com/PQeIZA6.png)

This CI pipeline can be more comprehensive, including deploying code to various environments, but that is beyond the scope of this project.


### Demo video

[Demo](https://imgur.com/a/WcVq3Cx)

![Demo gif](https://i.imgur.com/0QANQkS.gif)
