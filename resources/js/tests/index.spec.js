import { mount } from '@vue/test-utils';
import LeaderboardComponent from '../components/LeaderboardComponent.vue';

test('Sort players - Sort by name with no scores', () => {

	// b,c,a should sort to a,b,c based on the rules
	const localThis = { players: [
		{
			"name": "b",
			"score": 0
		},
		{
			"name": "c",
			"score": 0
		},
		{
			"name": "a",
			"score": 0
		}
	]};

	let expectedValues = [
		{
			"name": "a",
			"score": 0
		},
		{
			"name": "b",
			"score": 0
		},
		{
			"name": "c",
			"score": 0
		}
	];

	expect(LeaderboardComponent.computed.sortedPlayers.call(localThis)).toStrictEqual(expectedValues);
});

test('Sort players - Sort simply by score', () => {

	// a,b,c should sort to c,a,b based on the rules
	const localThis = { players: [
		{
			"name": "a",
			"score": 5
		},
		{
			"name": "b",
			"score": 2
		},
		{
			"name": "c",
			"score": 10
		}
	]};

	let expectedValues = [
		{
			"name": "c",
			"score": 10
		},
		{
			"name": "a",
			"score": 5
		},
		{
			"name": "b",
			"score": 2
		}
	];

	expect(LeaderboardComponent.computed.sortedPlayers.call(localThis)).toStrictEqual(expectedValues);
});


test('Sort players - Sort by name and then by scores', () => {

	// ac,ab,aa should sort to ab,ac,aa based on the rules
	const localThis = { players: [
		{
			"name": "ac",
			"score": 2
		},
		{
			"name": "ab",
			"score": 2
		},
		{
			"name": "aa",
			"score": 0
		}
	]};

	let expectedValues = [
		{
			"name": "ab",
			"score": 2
		},
		{
			"name": "ac",
			"score": 2
		},
		{
			"name": "aa",
			"score": 0
		}
	];

	expect(LeaderboardComponent.computed.sortedPlayers.call(localThis)).toStrictEqual(expectedValues);
});

